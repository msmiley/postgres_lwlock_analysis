## Postgres Lightweight Lock Analysis Tools

This is a collection of bpftrace programs and supporting scripts
for analyzing lightweight lock contention in Postgres, with a
particular focus on `LockManager` (formerly spelled `lock_manager`)
lightweight locks, which synchronize access to the shared-memory
lock table when acquiring or releasing heavyweight locks on relations.

For context, see the following:
* [link](https://gitlab.com/gitlab-com/gl-infra/scalability/-/issues/2301) - Details from the initial investigation (2023-04).
* [link](https://gitlab.com/gitlab-com/gl-infra/scalability/-/issues/2301#note_1357834678) - These tools were initially published here, and their initial use was in subsequent comments.
* [link](https://gitlab.com/gitlab-com/gl-infra/scalability/-/issues/2301#note_1391439437) - Conclusions from the initial analysis.
* [link](https://gitlab.com/gitlab-com/gl-infra/scalability/-/issues/2353) - Follow-up analysis (2023-10), after upgrading hardware (newer CPUs) and Postgres (12 to 14).
