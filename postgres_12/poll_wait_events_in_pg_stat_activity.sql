\pset pager off

select backend_type, wait_event_type, wait_event, count(1)
from pg_stat_activity
where state = 'active'
group by 1, 2, 3
order by 1, 2, 3
;

\watch 1
