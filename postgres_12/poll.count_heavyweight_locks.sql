\pset pager off

select
  now(),
  count(1) as num_locks,
  count(distinct l.pid) as num_xact,
  count(case when not l.fastpath then 1 end) as num_slowpath_locks,
  count(case when l.fastpath then 1 end) as num_fastpath_locks,
  count(distinct case when not l.fastpath then l.pid end) as num_xact_holding_slowpath_locks,
  count(case when l.mode in ( 'AccessShareLock', 'RowShareLock', 'RowExclusiveLock' ) then 1 end) as num_weak_locks,
  count(case when l.mode in ( 'ShareUpdateExclusiveLock' ) then 1 end) as num_sue_locks,
  count(case when l.mode in ( 'ShareLock', 'ShareRowExclusiveLock', 'ExclusiveLock', 'AccessExclusiveLock' ) then 1 end) as num_strong_locks
from pg_locks l
where
  l.locktype = 'relation'
  and l.pid != pg_backend_pid()
;

\watch 15
