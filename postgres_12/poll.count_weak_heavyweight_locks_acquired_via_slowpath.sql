/*
How many weak locks had to use slowpath because the transaction's fastpath slots were all already used?
Exclude weak locks that were ineligible for fastpath locking due to a concurrently held strong lock on the same relation.
*/

\pset pager off

with
  classified_locks as (
    select
      l.pid,
      l.relation,
      l.mode,
      l.fastpath,
      case when l.mode in ( 'AccessShareLock', 'RowShareLock', 'RowExclusiveLock' ) then true else false end as is_weak_lock,
      case when l.mode in ( 'ShareLock', 'ShareRowExclusiveLock', 'ExclusiveLock', 'AccessExclusiveLock' ) then true else false end) as is_strong_lock
    from
      pg_locks l
    where
      l.locktype = 'relation'
      and l.pid != pg_backend_pid()
  ),
  ineligible_relations as (
    select distinct relation
    from classified_locks
    where is_strong_lock
  )
select
  now(),
  c.relkind,
  n.nspname,
  c.relname,
  count(1) as num_locks,
  count(distinct l.pid) as num_xact,
  count(case when not l.fastpath then 1 end) as num_slowpath_locks,
  count(case when l.fastpath then 1 end) as num_fastpath_locks,
  count(distinct case when not l.fastpath then l.pid end) as num_xact_holding_slowpath_locks,
  count(case when l.mode in ( 'AccessShareLock', 'RowShareLock', 'RowExclusiveLock' ) then 1 end) as num_weak_locks,
  count(case when l.mode in ( 'ShareUpdateExclusiveLock' ) then 1 end) as num_sue_locks,
  count(case when l.mode in ( 'ShareLock', 'ShareRowExclusiveLock', 'ExclusiveLock', 'AccessExclusiveLock' ) then 1 end) as num_strong_locks
from
  classified_locks l
  inner join pg_class c on l.relation = c.oid
  inner join pg_namespace n on c.relnamespace = n.oid
where
  l.is_weak_lock
  and l.relation not in (
    select relation from ineligible_relations
  )
group by 1, 2, 3, 4
order by 5 desc, 1, 2, 3, 4
;

\watch 15
