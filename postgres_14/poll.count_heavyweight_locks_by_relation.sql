\pset pager off

select
  now(),
  c.relkind,
  n.nspname,
  c.relname,
  count(1) as num_locks,
  count(distinct l.pid) as num_xact,
  count(case when not l.fastpath then 1 end) as num_slowpath_locks,
  count(case when l.fastpath then 1 end) as num_fastpath_locks,
  count(distinct case when not l.fastpath then l.pid end) as num_xact_holding_slowpath_locks,
  count(case when l.mode in ( 'AccessShareLock', 'RowShareLock', 'RowExclusiveLock' ) then 1 end) as num_weak_locks,
  count(case when l.mode in ( 'ShareUpdateExclusiveLock' ) then 1 end) as num_sue_locks,
  count(case when l.mode in ( 'ShareLock', 'ShareRowExclusiveLock', 'ExclusiveLock', 'AccessExclusiveLock' ) then 1 end) as num_strong_locks
from
  pg_locks l
  inner join pg_class c on l.relation = c.oid
  inner join pg_namespace n on c.relnamespace = n.oid
where
  l.locktype = 'relation'
  and l.pid != pg_backend_pid()
group by 1, 2, 3, 4
order by 5 desc, 1, 2, 3, 4
;

\watch 15
