#!/bin/bash

set -o errexit
set -o pipefail

if [[ ! -f "db_oid_dictionary.all_relation_oids_to_names.psv" ]] ; then
    echo "Refreshing cache of OID dictionaries, to support post-processing of raw capture output."
    ./refresh_cached_postgres_oid_lookup_results.sh
fi

CAPTURE_ID="$( hostname -s ).$( date -u +%Y%m%d_%H%M%S )"

echo "Starting capture."
RAW_BPFTRACE_OUTFILE="results/count_lock_manager_lwlock_waits_by_relation_oid.$CAPTURE_ID.raw.out"
sudo ./bpftrace postgres14.count_lock_manager_lwlock_waits_by_relation_oid.bt 60 2> /dev/null | tee "$RAW_BPFTRACE_OUTFILE"

echo
echo "Finished capture."
echo "Starting post-processing."

echo
echo "Decoding OIDs to relation names."
DECODED_OIDS_OUTPUT="${RAW_BPFTRACE_OUTFILE%%.raw.out}.decoded_oids.out"
cat "$RAW_BPFTRACE_OUTFILE" | ./decode_oids_in_bpftrace_output.pl | tee "$DECODED_OIDS_OUTPUT" | tail -n10

echo
echo "Sum counts by table."
SUM_BY_TABLE_OUTPUT="${RAW_BPFTRACE_OUTFILE%%.raw.out}.sum_by_table.out"
cat "$RAW_BPFTRACE_OUTFILE" | ./decode_oids_in_bpftrace_output.sum_by_table.pl | tee "$SUM_BY_TABLE_OUTPUT" | tail -n10

echo
echo "Results files:"
echo " * Raw counts: $RAW_BPFTRACE_OUTFILE"
echo " * Decoded OIDs to relation names: $DECODED_OIDS_OUTPUT"
echo " * Sum counts by table: $SUM_BY_TABLE_OUTPUT"
