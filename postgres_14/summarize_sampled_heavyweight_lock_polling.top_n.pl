#!/usr/bin/perl

use strict;

main();

sub main {
    my $max_rows_to_show = configure_max_output_rows_per_report();
    my @reports = parse_input();
    my $report_id = 1;
    foreach my $report (@reports) {
        show_aggregation_results($report_id++, $report, $max_rows_to_show);
    }
}

sub configure_max_output_rows_per_report {
    # Limit output to top N rows for each report.
    my $max_rows_to_show = 9999;
    if (scalar(@ARGV) > 0 && $ARGV[0] =~ /^\d+$/) {
        $max_rows_to_show = shift @ARGV;
        usage() unless $max_rows_to_show > 0;
    }
    return $max_rows_to_show;
}

sub usage {
    die <<HERE;
Usage: $0 [max_rows_to_show_per_report] < input.psv
HERE
}

sub parse_input {
    my @input_fields = split /\|/, <>;
    my $slowpath_lock_count_by_relation = {};
    my $slowpath_lock_count_by_table = {};
    my $slowpath_xact_count_by_relation = {};
    my $slowpath_xact_count_by_table = {};
    my $strong_locks_count_by_relation = {};

    while (my $line = <>) {
        my %values;
        @values{@input_fields} = split /\|/, $line;
        sum_slowpath_lock_count_by_relation($slowpath_lock_count_by_relation, \%values);
        sum_slowpath_lock_count_by_relation($slowpath_lock_count_by_table, \%values) if $values{'relkind'} eq 'r';
        sum_xact_holding_slowpath_locks_by_relation($slowpath_xact_count_by_relation, \%values);
        sum_xact_holding_slowpath_locks_by_relation($slowpath_xact_count_by_table, \%values) if $values{'relkind'} eq 'r';
        sum_strong_locks_count_by_relation($strong_locks_count_by_relation, \%values);
    }

    my $slowpath_locks_per_xact_ratio_by_relation = ratio_for_matching_keys($slowpath_lock_count_by_relation, $slowpath_xact_count_by_relation);

    return (
        { desc => "Slowpath lock count, summed per relation",                                   data => $slowpath_lock_count_by_relation },
        { desc => "Slowpath lock count, summed per table (omits indexes)",                      data => $slowpath_lock_count_by_table },
        { desc => "Transactions holding slowpath locks, summed per relation",                   data => $slowpath_xact_count_by_relation },
        { desc => "Transactions holding slowpath locks, summed per table (omits indexes)",      data => $slowpath_xact_count_by_table },
        { desc => "Ratio of slowpath locks (of any mode) per transaction, summed per relation", data => $slowpath_locks_per_xact_ratio_by_relation },
        { desc => "Strong lock count (if any), summed per relation (forces slowpath locking)",  data => $strong_locks_count_by_relation },
    );
}

sub sum_slowpath_lock_count_by_relation
{
    my ($agg, $values) = @_;
    $agg->{ agg_key($values, qw/ relkind nspname relname /) } += $values->{'num_slowpath_locks'};
}

sub sum_xact_holding_slowpath_locks_by_relation
{
    my ($agg, $values) = @_;
    $agg->{ agg_key($values, qw/ relkind nspname relname /) } += $values->{'num_xact_holding_slowpath_locks'};
}

sub sum_strong_locks_count_by_relation
{
    my ($agg, $values) = @_;
    $agg->{ agg_key($values, qw/ relkind nspname relname /) } += $values->{'num_strong_locks'} if $values->{'num_strong_locks'} > 0;
}

sub ratio_for_matching_keys
{
    my ($hash1, $hash2) = @_;
    my $ratios = {};
    foreach my $key (keys %$hash1) {
        next unless (exists $hash2->{$key} && $hash2->{$key} != 0);
        $ratios->{$key} = sprintf "%0.2f", $hash1->{$key} / $hash2->{$key};
    }
    return $ratios;
}

sub agg_key {
    my ($values, @key_fields) = @_;
    return join(" ", map { defined($values->{$_}) ? $values->{$_} : "<undef>" } @key_fields);
}

sub show_aggregation_results {
    my ($report_id, $report, $max_rows_to_show) = @_;
    printf "Report %d: %s\n", $report_id, $report->{'desc'};
    my $i = 1;
    foreach my $agg_key (sort { $report->{'data'}{$b} <=> $report->{'data'}{$a} } keys %{$report->{'data'}}) {
        printf "%15s %s\n", $report->{'data'}{$agg_key}, $agg_key;
        last if $i++ >= $max_rows_to_show;
    }
    print "\n";
}
