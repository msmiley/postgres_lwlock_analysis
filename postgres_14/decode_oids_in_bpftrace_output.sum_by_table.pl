#!/usr/bin/perl

use strict;

use FileHandle;

main();

sub main
{
    my $relnames_by_oid = parse_oid_dictionary("db_oid_dictionary.all_relation_oids_to_names.psv");
    my $index_oid_to_table_oid = parse_index_to_table_dictionary("db_oid_dictionary.index_to_table.psv");
    aggregate_bpftrace_output($relnames_by_oid, $index_oid_to_table_oid);
}

# Parse dictionary file for mapping OID to relname.
sub parse_oid_dictionary
{
    my ($index_to_table_filename) = @_;
    return parse_dictionary_file(
        $index_to_table_filename,
        [ 'oid' ],
        [ 'relkind', 'relname' ],
        sub { my ($v) = @_; return "($v->{'relkind'}) $v->{'relname'}" },
    );
}

# Parse dictionary file for mapping indexes to tables.
sub parse_index_to_table_dictionary
{
    my ($index_to_table_filename) = @_;
    return parse_dictionary_file(
        $index_to_table_filename,
        [ 'index_oid' ],
        [ 'table_oid' ],
        sub { my ($v) = @_; return $v->{'table_oid'} },
    );
}

# Parse dictionary file.
sub parse_dictionary_file
{
    my ($dict_file, $key_fields, $value_fields, $value_generator_func) = @_;
    my $dict_fh = FileHandle->new($dict_file, "r")
        or die "ERROR: Cannot open file $dict_file: $!";

    my $header_line = <$dict_fh>;
    chomp($header_line);
    my @field_names = split /\|/, $header_line;
    foreach my $required_field (@$key_fields, @$value_fields) {
        die "Missing expected field '$required_field' in header line: $header_line\n"
            unless grep { $_ eq $required_field } @field_names;
    }

    my %dict;
    while (my $line = <$dict_fh>) {
        chomp($line);
        my %line_values;
        @line_values{@field_names} = split /\|/, $line, -1;
        my $dict_key = join("|", map { $line_values{$_} } @$key_fields);
        my $dict_val = $value_generator_func->(\%line_values);
        $dict{ $dict_key } = $dict_val;
    }
    close $dict_fh;

    return \%dict;
}

# Aggregate lines from STDIN that contain an index or table OID into table OID.  Other lines pass through unaltered.
# We do not aggregate toast tables into their parent table; only indexes are aggregated onto their tables.
# Input lines are expected to be a bpftrace map output where the Postgres relation OID is the only field of the BPF map's key.
# Example input line:
# @lock_manager_lwlock_wait_count_by_relation_oid[1249]: 1576
sub aggregate_bpftrace_output
{
    my ($relnames_by_oid, $index_oid_to_table_oid) = @_;

    # Parse incoming bpftrace output lines.  Aggregate BPF map entries, and print other lines unaltered.
    my %counts_by_map_name_and_table_oid;
    my $total_count;
    while (my $line = <>) {
        my ($map_name, $input_oid, $count) = ($line =~ /^\@(.*?)\[(\d+)\]\:\s+(\d+)$/);
        if (defined($input_oid)) {
            # Convert to table OID if it is an index.  Otherwise assume it is already a table OID.
            my $table_oid = $index_oid_to_table_oid->{$input_oid} || $input_oid;
            $map_name //= "";
            $counts_by_map_name_and_table_oid{$map_name}{$table_oid} += $count;
            $total_count += $count;
        } else {
            # Could not parse input line.  Print it as-is.
            print $line;
        }
    }

    # Print the aggregated BPF map.
    foreach my $map_name (sort keys %counts_by_map_name_and_table_oid) {
        print "\n";
        my $counts_by_table_oid = $counts_by_map_name_and_table_oid{$map_name};
        foreach my $table_oid (sort { $counts_by_table_oid->{$a} <=> $counts_by_table_oid->{$b} } keys %$counts_by_table_oid) {
            printf "\@%s[%s, '%s']: %d = %0.1f%%\n",
                $map_name,
                $table_oid,
                $relnames_by_oid->{$table_oid},
                $counts_by_table_oid->{$table_oid},
                (100 * $counts_by_table_oid->{$table_oid} / $total_count);
        }
    }
}
