#!/bin/bash

set -e

OID_TO_RELATION_NAME_DICT="db_oid_dictionary.all_relation_oids_to_names.psv"
echo "Generating dictionary of OIDs to relation names: $OID_TO_RELATION_NAME_DICT" 1>&2
sudo gitlab-psql -AXq <<HERE > "$OID_TO_RELATION_NAME_DICT"
select
  c.oid,
  c.relkind,
  n.nspname || '.' || c.relname as relname
from
  pg_class c
  join pg_namespace n on c.relnamespace = n.oid
order by 1
;
HERE

INDEX_TO_TABLE_DICT="db_oid_dictionary.index_to_table.psv"
echo "Generating dictionary of indexes to tables: $INDEX_TO_TABLE_DICT" 1>&2
sudo gitlab-psql -AXq <<HERE > "$INDEX_TO_TABLE_DICT"
select
  i.oid as index_oid,
  r.oid as table_oid,
  i.relkind as index_relkind,
  r.relkind as table_relkind,
  n.nspname || '.' || i.relname as index_relname,
  n.nspname || '.' || r.relname as table_relname
from
  pg_index x
  join pg_class i on x.indexrelid = i.oid
  join pg_class r on x.indrelid = r.oid
  join pg_namespace n on i.relnamespace = n.oid
where
  r.relkind in ('r', 'm', 'p')
  and i.relkind in ('i', 'I')
order by 1
;
HERE

