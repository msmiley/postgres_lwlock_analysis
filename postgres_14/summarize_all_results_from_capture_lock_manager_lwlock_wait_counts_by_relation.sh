#!/bin/bash

NUM_ROWS_PER_REPORT=10
HOSTNAME_TO_REPORT=$( hostname -s )

echo "Top $NUM_ROWS_PER_REPORT LockManager stall counts, summed by relation (ascending order):"
cat results/count_lock_manager_lwlock_waits_by_relation_oid.${HOSTNAME_TO_REPORT}.*.raw.out \
    | ./decode_oids_in_bpftrace_output.sum_by_relation.pl \
    | tail -n "$NUM_ROWS_PER_REPORT"

echo
echo "Top $NUM_ROWS_PER_REPORT LockManager stall counts, summed by table (ascending order):"
cat results/count_lock_manager_lwlock_waits_by_relation_oid.${HOSTNAME_TO_REPORT}.*.raw.out \
    | ./decode_oids_in_bpftrace_output.sum_by_table.pl \
    | tail -n "$NUM_ROWS_PER_REPORT"

echo
echo "For context, top $NUM_ROWS_PER_REPORT tables by index count (ascending order):"
cat db_oid_dictionary.index_to_table.psv | cut -d'|' -f6 | sort | uniq -c | sort -n | tail -n "$NUM_ROWS_PER_REPORT"
