#!/usr/bin/perl

use strict;

use FileHandle;

main();

sub main
{
    my $relnames_by_oid = parse_oid_dictionary("db_oid_dictionary.all_relation_oids_to_names.psv");
    annotate_bpftrace_output($relnames_by_oid);
}

# Parse dictionary file for mapping OID to relname.
sub parse_oid_dictionary
{
    my ($oid_translations_filename) = @_;
    my $dict_fh = FileHandle->new($oid_translations_filename, "r")
        or die "ERROR: Cannot open file $oid_translations_filename: $!";

    my $header_line = <$dict_fh>;
    chomp($header_line);
    my @field_names = split /\|/, $header_line;
    die "Missing expected fields in header line: $header_line\n" unless join("|", @field_names) eq "oid|relkind|relname";

    my %relnames_by_oid;
    while (my $line = <$dict_fh>) {
        chomp($line);
        my %dict_entry;
        @dict_entry{@field_names} = split(/\|/, $line);
        $relnames_by_oid{ $dict_entry{'oid'} } = "($dict_entry{'relkind'}) $dict_entry{'relname'}";
    }
    close $dict_fh;

    return \%relnames_by_oid;
}

# Annotate lines from STDIN that contain an OID.
# Input lines are expected to be a bpftrace map output where the Postgres relation OID is the first numeric field of the BPF map's key.
# Example input line:
# @lock_manager_lwlock_wait_count_by_relation_oid[1249]: 1576
sub annotate_bpftrace_output
{
    my ($relnames_by_oid) = @_;
    while (my $line = <>) {
        $line =~ s/^(\@.*?\[.*?\b)(\d+)(\b.*?\]\:)/${1}${2},'@{[ $relnames_by_oid->{$2} ]}'${3}/;
        print $line;
    }
}
